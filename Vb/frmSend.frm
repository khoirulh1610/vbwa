VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSend 
   Caption         =   "Send Image"
   ClientHeight    =   4020
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   4965
   LinkTopic       =   "Form1"
   ScaleHeight     =   4020
   ScaleWidth      =   4965
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton Option2 
      Caption         =   "Image / File"
      Height          =   195
      Left            =   2040
      TabIndex        =   8
      Top             =   240
      Width           =   1815
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Chat / Text"
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Top             =   240
      Width           =   1455
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   375
      Left            =   2280
      TabIndex        =   6
      Top             =   2520
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "HH:mm"
      Format          =   41287682
      CurrentDate     =   44065
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   240
      TabIndex        =   5
      Top             =   2520
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "yyyy-MM-dd"
      Format          =   41287683
      CurrentDate     =   44065
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Kirim"
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   3360
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "..."
      Height          =   255
      Left            =   4320
      TabIndex        =   3
      Top             =   1080
      Width           =   375
   End
   Begin VB.TextBox Text2 
      Height          =   855
      Left            =   240
      TabIndex        =   2
      Top             =   1560
      Width           =   4455
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   240
      TabIndex        =   1
      Top             =   1080
      Width           =   4095
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   4455
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   7080
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmSend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
    Me.CommonDialog1.Filter = "Graphic Files (*.bmp;*.gif;*.jpg)|*.bmp;*.gif;*.jpg"
    Me.CommonDialog1.ShowOpen
    Me.Text1.Text = Me.CommonDialog1.FileName
End Sub

Private Sub Command2_Click()
    
        If (Me.Option2.Value = True) Then
            If (Trim(Me.Combo1.Text) = "" Or Trim(Me.Text1.Text) = "" Or Trim(Me.Text2.Text) = "") Then
                MsgBox "Data belum lengkap", vbCritical, "Peringatan"
                Exit Sub
            End If
        Else
            If (Trim(Me.Combo1.Text) = "" Or Trim(Me.Text2.Text) = "") Then
                MsgBox "Data belum lengkap", vbCritical, "Peringatan"
                Exit Sub
            End If
        End If
    
        Dim ctype As String
        Dim sent_at As String
        ctype = chat
        If (Me.Option2.Value = True) Then ctype = "Image"
        sent_at = Format(Me.DTPicker1.Value, "yyyy-MM-dd") & " " & Format(Me.DTPicker2.Value, "HH:mm")
        ex_query ("insert into chats(chatid,message,file,chat_type,type,created_at,sent_at)values('" & Me.Combo1.Text & "','" & Me.Text2.Text & "','" & Replace(Me.Text1.Text, "\", "\\") & "','out','" & ctype & "',now(),'" & sent_at & "')")
        Me.Text1.Text = ""
        Me.Text2.Text = ""
        Me.DTPicker1.Value = Format(Now, "yyyy-mm-dd")
        Me.DTPicker2.Value = Format(Now, "HH:mm")
End Sub

Private Sub Form_Load()
    Call tampil
    Me.DTPicker1.Value = Format(Now, "yyyy-mm-dd")
    Me.DTPicker2.Value = Format(Now, "HH:mm")
    Me.Option1.Value = True
    Me.Option2.Value = False
End Sub

Sub tampil()
    Call buat_koneksi
    Dim LV As ListItem
    Dim SQL As String
    Dim Reset As New ADODB.Recordset
    
    SQL = "SELECT chatid,pushname FROM chats group by chatid"
    Set Reset = conn.Execute(SQL)
    Me.Combo1.clear
    Do While Not Reset.EOF
        Me.Combo1.AddItem Reset!chatid
    Reset.MoveNext
    Loop
    Set Reset = Nothing
    conn.Close
End Sub

Private Sub Option1_Click()
    If Me.Option1.Value = True Then
        Me.Text1.Enabled = False
        Me.Command1.Enabled = False
    End If
End Sub

Private Sub Option2_Click()
    If Me.Option2.Value = True Then
        Me.Text1.Enabled = True
        Me.Command1.Enabled = True
    End If
End Sub
