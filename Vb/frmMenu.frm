VERSION 5.00
Begin VB.MDIForm frmMenu 
   BackColor       =   &H8000000C&
   Caption         =   "Whatsapp BOT"
   ClientHeight    =   8145
   ClientLeft      =   225
   ClientTop       =   855
   ClientWidth     =   15195
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Menu mnMenu 
      Caption         =   "Menu"
      Begin VB.Menu mnAutoreply 
         Caption         =   "Auto Reply"
      End
   End
   Begin VB.Menu mnChats 
      Caption         =   "Chats"
      Enabled         =   0   'False
      Visible         =   0   'False
   End
   Begin VB.Menu mnInbox 
      Caption         =   "Inbox"
   End
   Begin VB.Menu mnOutbox 
      Caption         =   "Outbox"
   End
   Begin VB.Menu mnSetting 
      Caption         =   "Setting"
      Begin VB.Menu mnQrcode 
         Caption         =   "Qrcode"
      End
   End
End
Attribute VB_Name = "frmMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub MDIForm_Load()
    'buat_koneksi
    INIWrite "mysql", "host", "140.0.94.43", "data.ini"
    
End Sub

Private Sub mnAutoreply_Click()
    frmAutoReply.Show
    
End Sub

Private Sub mnChats_Click()
    frmChats.Show
End Sub

Private Sub mnInbox_Click()
    frmInbox.Show
End Sub

Private Sub mnOutbox_Click()
    frmOutbox.Show
End Sub

Private Sub mnQrcode_Click()
    frmQrcode.Show
End Sub
