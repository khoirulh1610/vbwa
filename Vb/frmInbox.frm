VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmInbox 
   Caption         =   "Inbox"
   ClientHeight    =   7980
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   13965
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7980
   ScaleWidth      =   13965
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   3720
      Top             =   600
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Refresh"
      Height          =   495
      Left            =   1800
      TabIndex        =   2
      Top             =   120
      Width           =   1575
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Reply"
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1575
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   7095
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   13815
      _ExtentX        =   24368
      _ExtentY        =   12515
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "id"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "ChatID"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Message"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "CreatedAt"
         Object.Width           =   2540
      EndProperty
   End
End
Attribute VB_Name = "frmInbox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    If (Me.ListView1.SelectedItem.Text <> "") Then
        frmSend.Combo1.Text = Me.ListView1.SelectedItem.SubItems(1)
        frmSend.Show
    End If
End Sub

Private Sub Command2_Click()
    Call tampil
End Sub

Private Sub Form_Load()
    Call tampil
End Sub

Sub tampil()
    Call buat_koneksi
    Dim LV As ListItem
    Dim SQL As String
    Dim Reset As New ADODB.Recordset
    
    SQL = "SELECT * FROM chats where chat_type='in' order by id desc"
    Set Reset = conn.Execute(SQL)
    ListView1.ListItems.clear
    Do While Not Reset.EOF
    
    Set LV = ListView1.ListItems.Add(, , Reset.Fields(0))
    LV.SubItems(1) = Reset!chatid & ""
    LV.SubItems(2) = Reset!message & ""
    LV.SubItems(3) = Reset!created_at & ""
    Reset.MoveNext
    Loop
    Set Reset = Nothing
    conn.Close
End Sub


Private Sub Form_Resize()
    Me.ListView1.Left = 0
    Me.ListView1.Width = Me.Width - 300
    Me.ListView1.Height = Me.Height - Me.ListView1.Top - 200
End Sub

Private Sub Timer1_Timer()
    tampil
End Sub
