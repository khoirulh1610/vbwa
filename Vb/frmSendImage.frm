VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSendImage 
   Caption         =   "Send Image"
   ClientHeight    =   4020
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   7515
   LinkTopic       =   "Form1"
   ScaleHeight     =   4020
   ScaleWidth      =   7515
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton Option2 
      Caption         =   "Image / File"
      Height          =   195
      Left            =   2040
      TabIndex        =   8
      Top             =   240
      Width           =   1815
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Chat / Text"
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Top             =   240
      Width           =   1455
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   375
      Left            =   2280
      TabIndex        =   6
      Top             =   2520
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "HH:mm"
      Format          =   38928386
      CurrentDate     =   44065
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   360
      TabIndex        =   5
      Top             =   2520
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "yyyy-MM-dd"
      Format          =   38928387
      CurrentDate     =   44065
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Kirim"
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   3360
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "..."
      Height          =   255
      Left            =   4320
      TabIndex        =   3
      Top             =   1080
      Width           =   375
   End
   Begin VB.TextBox Text2 
      Height          =   855
      Left            =   240
      TabIndex        =   2
      Top             =   1560
      Width           =   4455
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   240
      TabIndex        =   1
      Top             =   1080
      Width           =   4095
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   4455
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3840
      Top             =   2760
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmSendImage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
    Me.CommonDialog1.Filter = "Graphic Files (*.bmp;*.gif;*.jpg)|*.bmp;*.gif;*.jpg"
    Me.CommonDialog1.ShowOpen
    Me.Text1.Text = Me.CommonDialog1.FileName
End Sub

Private Sub Command2_Click()
    If (Me.Combo1.Text <> "" And Me.Text1.Text <> "" And Me.Text2.Text <> "") Then
        ex_query ("insert into chats(chatid,message,file,chat_type,type,created_at)values('" & Me.Combo1.Text & "','" & Me.Text2.Text & "','" & Replace(Me.Text1.Text, "\", "\\") & "','out','image',now())")
        Me.Text1.Text = ""
        Me.Text2.Text = ""
    Else
        MsgBox "Data belum lengkap", vbCritical, "Peringatan"
    End If
    
End Sub

Private Sub Form_Load()
    Call tampil
    Me.DTPicker1.Value = Format(Now, "yyyy-mm-dd")
    Me.DTPicker2.Value = Format(Now, "HH:mm")
End Sub

Sub tampil()
    Call buat_koneksi
    Dim LV As ListItem
    Dim SQL As String
    Dim Reset As New ADODB.Recordset
    
    SQL = "SELECT chatid,pushname FROM chats group by chatid"
    Set Reset = conn.Execute(SQL)
    Me.Combo1.clear
    Do While Not Reset.EOF
        Me.Combo1.AddItem Reset!chatid
    Reset.MoveNext
    Loop
    Set Reset = Nothing
    conn.Close
End Sub

