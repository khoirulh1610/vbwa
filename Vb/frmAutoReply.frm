VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAutoReply 
   Caption         =   "Setting Auto Reply"
   ClientHeight    =   5925
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   13710
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5925
   ScaleWidth      =   13710
   Begin VB.CommandButton Command2 
      Caption         =   "Delete"
      Height          =   495
      Left            =   1320
      TabIndex        =   6
      Top             =   2160
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Save"
      Height          =   495
      Left            =   360
      TabIndex        =   5
      Top             =   2160
      Width           =   855
   End
   Begin VB.TextBox Text2 
      Height          =   855
      Left            =   360
      TabIndex        =   2
      Text            =   "Text2"
      Top             =   1200
      Width           =   12735
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   360
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   360
      Width           =   1695
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   2895
      Left            =   360
      TabIndex        =   0
      Top             =   2760
      Width           =   13575
      _ExtentX        =   23945
      _ExtentY        =   5106
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "ID"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "PERINTAH"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "RESPONSE"
         Object.Width           =   14111
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "CREATED AT"
         Object.Width           =   5292
      EndProperty
   End
   Begin VB.Label Label2 
      Caption         =   "Response"
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   840
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Perintah"
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   0
      Width           =   1575
   End
End
Attribute VB_Name = "frmAutoReply"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Sub tampil()
    Call buat_koneksi
    Dim LV As ListItem
    Dim SQL As String
    Dim Reset As New ADODB.Recordset
    
    SQL = "SELECT * FROM wabots"
    Set Reset = conn.Execute(SQL)
    ListView1.ListItems.clear
    Do While Not Reset.EOF
    
    Set LV = ListView1.ListItems.Add(, , Reset.Fields(0))
    LV.SubItems(1) = Reset!perintah & ""
    LV.SubItems(2) = Reset!response & ""
    LV.SubItems(3) = Reset!created_at & ""
    Reset.MoveNext
    Loop
    Set Reset = Nothing
    conn.Close
End Sub

Private Sub Command1_Click()
    If (Me.Text1.Text <> "" And Me.Text2.Text <> "") Then
        ex_query ("insert into wabots(perintah,response,created_at)values('" & Me.Text1.Text & "','" & Me.Text2.Text & "',now())")
        Call clear
        Call tampil
    End If
End Sub

Private Sub Command2_Click()
    If Me.ListView1.SelectedItem.Text > 0 Then
        If MsgBox("Yakin Hapus ? " & vbCrLf & Me.ListView1.SelectedItem.SubItems(1), vbQuestion, "Hapus") = vbYes Then
            ex_query ("delete from wabots where id=" & Me.ListView1.SelectedItem.Text)
            Call clear
            Call tampil
        End If
    End If
End Sub

Private Sub Form_Load()
    Call clear
    Call tampil
End Sub

Private Sub Form_Resize()
    Me.ListView1.Left = 0
    Me.ListView1.Width = Me.Width - 300
    Me.ListView1.Height = Me.Height - Me.ListView1.Top - 200
End Sub

Private Sub clear()
    Me.Text1.Text = ""
    Me.Text2.Text = ""
End Sub


