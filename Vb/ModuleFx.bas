Attribute VB_Name = "ModuleFx"
Public conn As New ADODB.Connection

Public Function ConnString() As String
    Dim db_name As String
    Dim db_server As String
    Dim db_port As String
    Dim db_user As String
    Dim db_pass As String
    db_name = "vbwa" ' ganti dengan nama database anda
    db_server = "140.0.94.43" 'ganti jika server anda ada di komputer lain
    db_port = "3306"    'default port is 3306
    db_user = "vbwa"    'sebaiknya pakai username lain.
    db_pass = "vbwa123456"
    ConnString = "DRIVER={MySQL ODBC 3.51 Driver};SERVER=" & db_server & ";DATABASE=" & db_name & ";UID=" & db_user & ";PWD=" & db_pass & ";PORT=" & db_port & ";OPTION=3"
End Function
Sub buat_koneksi()
    On Error GoTo buat_koneksi_Error
    conn.ConnectionString = ConnString()
    With conn
        .ConnectionString = ConnString
        .Open
    End With
    '___________________________________________________________
    On Error GoTo 0
    Exit Sub
buat_koneksi_Error:
    MsgBox "Ada kesalahan dengan server, periksa apakah server sudah berjalan !" & vbCrLf & Error, vbInformation, "Cek Server"
End Sub

Public Function DataRs(ByVal str As String) As ADODB.Recordset
    On Error GoTo buat_koneksi_Error
    Dim rs As ADODB.Recordset
    conn.ConnectionString = ConnString()
    conn.Open
    Set rs = conn.Execute(str)
    conn.Close
    Set DataRs = rs
    '___________________________________________________________
    On Error GoTo 0
    Exit Function
buat_koneksi_Error:
    MsgBox "Ada kesalahan dengan server, periksa apakah server sudah berjalan !" & vbCrLf & Error, vbInformation, "Cek Server"
End Function

Public Sub ex_query(ByVal str As String)
    On Error GoTo buat_koneksi_Error
    With conn
        .ConnectionString = ConnString()
        .Open
        .Execute (str)
        .Close
    End With
    '___________________________________________________________
    On Error GoTo 0
    Exit Sub
buat_koneksi_Error:
    MsgBox "Ada kesalahan dengan server, periksa apakah server sudah berjalan !" & vbCrLf & Error, vbInformation, "Cek Server"
End Sub
